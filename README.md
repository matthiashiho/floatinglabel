## Configurable Floating Label for Input and Textarea Elements

Set the following variables in the :root Selector of the .css file according your configuration.

```
/* font-size of input and textarea element*/
--floating-size: 16px;
/* color of input and textarea */
--floating-color:#000;
/* background color of input/textarea element */
--floating-backcolor: #fff;
/* color of label while it's input/textarea element is not in focus */
--floating-unselect: #999;
/* color of label while it's input/textarea element is in focus */
--floating-select:rgb(81, 203, 238 ,1 );
/* border-width of input element with a line */
--floating-input-line: 1px;
/* border-width of input in :focus */
--floating-input-border: 0px;
/* border-width of textarea in :focus */
--floating-textarea-border: 1px;
/* border width of line in case element is :invalid */
--floating-error-line: 1px;
/* color of line in case element is :invalid */
--floating-error-color: red;
```
***
## HTML - Input

```
<div class='floating-label floating-label-input'>
	<input id="name" name="name" type='text' value='' placeholder="Name">
	<label for="name">Name:</label>
</div>
```

## HTML - Textarea
```
<div class='floating-label floating-label-textarea'>
	<textarea id="message" name="message" placeholder="Message"></textarea>
	<label for="message">Message:</label>
</div>
```

***
See index.html for an example how to use css-classes on `div` elements and how to place `input` and 
`label` tags whithin.



